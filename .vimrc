" Tab / Indention Setup
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
filetype indent on

" Use system clipboard by default
set clipboard^=unnamed,unnamedplus

" Line Numbers
set number
"set relativenumber

" Other Settings
set incsearch
set laststatus=2
set hidden
set nowrap
set scrolloff=10
set ttimeoutlen=50
set notitle
set ignorecase
set smartcase

" set runtimepath^=~/.vim/bundle/ctrlp.vim

" Remove all trailing whitespace before saving
autocmd BufWritePre * %s/\s\+$//e

" Auto update files modified outside of Vim (can't get this working reliably yet)
set autoread
autocmd FocusGained,BufEnter,FileChangedShell * :checktime

" Put swap files out of the way
if !isdirectory($HOME.'/.vim/swap')
    call mkdir($HOME.'/.vim/swap', "p")
endif
set directory=$HOME/.vim/swap//

" Plugins
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', {'do': './install --bin'}
Plug 'junegunn/fzf.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'tpope/vim-fugitive'
Plug 'derekwyatt/vim-fswitch'
Plug 'majutsushi/tagbar'
Plug 'jnurmine/zenburn'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'kien/ctrlp.vim'
Plug 'amerlyq/vim-focus-autocmd'
Plug 'tpope/vim-abolish'
call plug#end()

" let g:airline_skip_empty_sections = 1
" let g:airline#extnsions#tagbar#enabled = 1
" let g:airline_powerline_fonts = 1

" Put all ctags generated 'tags' files in one place - out of the way
let g:gutentags_cache_dir=$HOME.'/.ctags'

" Underlines look ugly
let g:zenburn_disable_Label_underline=1

let g:ctrlp_working_path_mode = 'a'

" Theme
colors zenburn

" Key Mappings
map <Space>f :Files<CR>
map <Space>b :Buffers<CR>
map <Space>t :Tags<CR>
map <Space>o :FSHere<CR>
map <Space>' :TagbarOpenAutoClose<CR>
map <Space>g :Ag<Space>
map <F6> :set spell!<CR>
" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>s
