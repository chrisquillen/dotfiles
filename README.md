# dotfiles

## Create User bin Directory
* mkdir ~/.local/bin
* shutdown -r now #A restart is required to get this directory on the PATH (see ~/.profile)

## Clone Repo

* mkdir ~/proj
* cd ~/proj
* git clone http://bitbucket.org/chrisquillen/dotfiles
* cd dotfiles
* git config user.name "Chris Quillen"
* git config user.email "cquillen@me.com"

## Install
* sudo apt install ansible
* ansible-playbook playbook.yml -K

