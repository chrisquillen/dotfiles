set nocompatible

" Filetype Support
filetype plugin indent on

" Tab / Indention Setup
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" Line Numbers
set number
" set relativenumber

" Better tab-completion for filenames
set wildmode=longest,list,full
set wildmenu

" Other Settings
set incsearch
set laststatus=2
set hidden
set nowrap
set scrolloff=10
set sidescrolloff=10
set ttimeoutlen=10
set notitle
set formatoptions+=j "delete comment character(s) when joining commented lines (J)
set backspace=indent,eol,start
set lazyredraw
set completeopt=menuone,noselect
set ignorecase
set smartcase

" Enable Mouse Support
set mouse=a

" Use system clipboard by default
set clipboard+=unnamedplus

" Enable persistent undo
set undofile

" Plugins
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'derekwyatt/vim-fswitch'
Plug 'jnurmine/zenburn'
Plug 'tpope/vim-vinegar'
Plug 'blueyed/vim-diminactive'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'edlanglois/vim-qrc'
Plug 'axelf4/vim-strip-trailing-whitespace'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'tpope/vim-abolish'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'
Plug 'folke/which-key.nvim'
Plug 'machakann/vim-highlightedyank'
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-compe'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'kyazdani42/nvim-web-devicons'
Plug 'preservim/tagbar'
Plug 'lewis6991/spellsitter.nvim'
Plug 'ludovicchabant/vim-gutentags'
call plug#end()

" Put all ctags generated 'tags' files in one place - out of the way
let g:gutentags_cache_dir=$HOME.'/.ctags'

" set highlightedyank time to 300m
let g:highlightedyank_highlight_duration = 300

" Theme
let g:zenburn_disable_Label_underline=1 " Underlines look ugly
colors zenburn

" Key Mappings
nnoremap <Space> <NOP>
nnoremap <silent> <Space>o :FSHere<CR>
nnoremap <silent> <Space>r :set relativenumber!<CR>
nnoremap <silent> <Space>s :set spell!<CR>
"nnoremap <silent> <Space>h :nohlsearch<CR>
nnoremap <silent> <Space>. :e $MYVIMRC<CR>
nnoremap <silent> <esc> <esc>:nohlsearch<CR><esc>

nnoremap <Space>f <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <Space>F <cmd>lua require('telescope.builtin').find_files( { find_command = { 'rg', '--files', '--no-ignore', '--no-ignore-dot' } } )<cr>
nnoremap <Space>lg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <Space>b <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <Space>t <cmd>lua require('telescope.builtin').treesitter()<cr>
nnoremap <Space>G <cmd>lua require('telescope.builtin').grep_string()<cr>
nnoremap <Space>g <cmd>lua require('telescope.builtin').grep_string({search = vim.fn.input("Grep For > ")})<cr>
nnoremap <Space>q <cmd>lua require('telescope.builtin').quickfix()<cr>

" I never go into Ex mode on purpose
nnoremap <silent> Q <nop>

" Quickfix navigation.  Re-center line and open any folds.
nnoremap <C-j> :cnext<CR>zzzv
nnoremap <C-k> :cprev<CR>zzzv

" Resize vertical splits.
nnoremap <C-h> :vertical resize -5<CR>
nnoremap <C-l> :vertical resize +5<CR>

" Make Y behave like C and D, that is, copy to end of line (not entire line).
nnoremap Y y$

" Search results navigation. Re-center line and open any folds.
nnoremap n nzzzv
nnoremap N Nzzzv

" Line concatenation. Keep cursor where it is.
nnoremap J mzJ`z

" Move blocks of code.
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Setup statusline
" start of default statusline
set statusline=%f\ %h%w%m%r
" Show current function name from tagbar
set statusline+=%{tagbar#currenttag('\[%s\]','','f')}
"set statusline+=%{nvim_treesitter#statusline()}
" end of default statusline (with ruler)
"set statusline+=%=%(%l,%c%V\ %=\ %P%)
set statusline+=%=%y

lua << EOF
local nvim_lsp = require('lspconfig')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  -- buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  -- buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  -- buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  -- buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  -- buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  -- buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  -- buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  -- buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  -- buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  -- buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  -- buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  -- buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  --buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)

end

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
-- NOTE: pyright must first be installed via npm (sudo npm i -g pyright).  Node version must be latest: https://github.com/nodesource/distributions/blob/master/README.md#snapinstall
local servers = { "clangd", "rust_analyzer", "cmake", "pyright" }
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
  }
end

EOF

lua vim.lsp.handlers["textDocument/publishDiagnostics"] = function() end

let g:compe = {}
let g:compe.enabled = v:true
let g:compe.autocomplete = v:true
let g:compe.debug = v:false
let g:compe.min_length = 1
let g:compe.preselect = 'enable'
let g:compe.throttle_time = 80
let g:compe.source_timeout = 200
let g:compe.resolve_timeout = 800
let g:compe.incomplete_delay = 400
let g:compe.max_abbr_width = 100
let g:compe.max_kind_width = 100
let g:compe.max_menu_width = 100
let g:compe.documentation = v:true

let g:compe.source = {}
let g:compe.source.path = v:true
let g:compe.source.buffer = v:true
let g:compe.source.calc = v:true
let g:compe.source.nvim_lsp = v:true
let g:compe.source.nvim_lua = v:true
let g:compe.source.vsnip = v:true
let g:compe.source.ultisnips = v:true
let g:compe.source.luasnip = v:true
let g:compe.source.emoji = v:false

let g:indent_blankline_char = '┊'

lua <<EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = {"c", "cpp", "cmake", "rust", "python", "toml", "yaml", "bash", "css", "html", "javascript", "json"},
  highlight = {
    enable = true,
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}

local actions = require('telescope.actions')
require('telescope').setup{
  defaults = {
    vimgrep_arguments = {
      'rg',
      '--color=never',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case'
    },
    prompt_prefix = "> ",
    selection_caret = "> ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "descending",
    layout_strategy = "vertical",
    layout_config = {
      horizontal = {
        mirror = false,
      },
      vertical = {
        mirror = false,
      },
    },
    file_sorter =  require'telescope.sorters'.get_fuzzy_file,
    file_ignore_patterns = {},
    generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
    winblend = 0,
    border = {},
    borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
    color_devicons = true,
    use_less = true,
    path_display = {},
    set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
    file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
    grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
    qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,

    -- Developer configurations: Not meant for general override
    buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker,

    -- ESC will close telescope, rather than go into normal mode
    mappings = {
      i = {
        ["<esc>"] = actions.close,
        ["<C-w>"] = actions.send_selected_to_qflist,
        ["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
      },
    },
  },

  pickers = {
    find_files = {
      -- Follow symlinks
      follow = true,
    },
    buffers = {
      sort_lastused = true,
      sort_mru = true,
    },
  },

  extensions = {
    fzf = {
      fuzzy = true,                    -- false will only do exact matching
      override_generic_sorter = false, -- override the generic sorter
      override_file_sorter = true,     -- override the file sorter
      case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
                                       -- the default case_mode is "smart_case"
    }
  }
}
require('telescope').load_extension('fzf')
require('spellsitter').setup({
  hl = 'SpellBad',
  captures = {'comment', 'string'},
  })

require("which-key").setup()


EOF

